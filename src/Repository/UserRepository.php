<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Translation\TranslatorInterface;

class UserRepository extends BaseRepository
{
    private $translator;

    public function __construct(RegistryInterface $registry, TranslatorInterface $translator)
    {
        $this->translator = $translator;
        parent::__construct($registry, User::class, 'u');
    }

    public function findOneByUsernameOrEmail(string $username)
    {
        $q = $this->createQueryBuilder($this->_alias);
        $q->where($q->expr()->eq($this->_alias.'.username', $q->expr()->literal($username)))
            ->orWhere($q->expr()->eq($this->_alias.'.email', $q->expr()->literal($username)));

        $res = $q->getQuery()->getResult();
        if (is_array($res) && 1 === count($res)) {
            return $res[0];
        } else {
            throw new UsernameNotFoundException($this->translator->trans('error.username_not_found', [
                '%pseudo%' => $username
            ]));
        }
    }
}
