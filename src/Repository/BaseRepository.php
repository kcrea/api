<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 04/08/2017
 * Time: 09:19
 */

namespace App\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

abstract class BaseRepository extends ServiceEntityRepository
{
    protected $_alias;

    public function __construct(RegistryInterface $registry, $className, $alias)
    {
        $this->_alias = $alias;
        parent::__construct($registry, $className);
    }

    public function getFirst()
    {
        $q = $this->createQueryBuilder($this->_alias);
        $q->setMaxResults(1);
        return $q->getQuery()->getResult();
    }

    public function getCount($filters = [], $q = null)
    {
        $q = ($q != null) ? $q : $this->createQueryBuilder($this->_alias);
        $q->select("COUNT({$this->_alias})");
        $this->addFilters($q, $filters);
        return $q->getQuery()->getSingleResult();
    }

    public function getAll()
    {
        $q = $this->createQueryBuilder($this->_alias)
            ->select($this->_alias);
        return $q->getQuery()->getResult(Query::HYDRATE_OBJECT);
    }

    /**
     * Retourne un tableau contenant les dépendences d'une entité sous la forme [entite][attribut]
     * @param $entity
     * @return array
     */
    public function getAllDependencies($entity)
    {
        $dependenciesArray = $entity->getDependenciesFields();
        $q = $this->createQueryBuilder($this->_alias);
        $q->resetDQLPart('select');
        foreach ($dependenciesArray as $attribute) {
            $q->addSelect("IDENTITY({$this->_alias}.$attribute) as $attribute");
        }
        return $q->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function getDependency($dependencyField)
    {
        $q = $this->createQueryBuilder($this->_alias);
        $q->resetDQLPart('select');
//        V1
        $q->addSelect("DISTINCT IDENTITY({$this->_alias}.$dependencyField) as $dependencyField");
//       V1
        return $q->getQuery()->getResult(Query::HYDRATE_OBJECT);
    }

    public function getDistinct($dependencyField, $params = [], $q = null){
        $q = ($q != null) ? $q : $this->createQueryBuilder($this->_alias);
        $q->resetDQLPart('select');
        $q->addSelect("DISTINCT ({$this->_alias}.$dependencyField)");
        $q->andWhere("{$this->_alias}.$dependencyField IS NOT NULL");
        $this->addFilters($q, $params);
        return $q->getQuery()->getResult(Query::HYDRATE_OBJECT);
    }

    public function filter($start, $nb, $filters, $q = null)
    {
        $q = ($q != null) ? $q : $this->createQueryBuilder($this->_alias);

        //$i=1;
        //$totalfilters = count($filters);
        //$filtreString = '';
        //$flagOrderBy = false;
//        $q = $this->createQueryBuilder($this->_alias)
        $q->select($this->_alias);


        if (isset($filters['q'])) {
            $queryFilter = $filters['q'];
            unset($filters['q']);
            $this->elaboratedFilter($q, $queryFilter);
        }
        $this->addFilters($q, $filters);

        $q->setMaxResults($nb)
            ->setFirstResult($start);

        return $q->getQuery()->getResult(Query::HYDRATE_OBJECT);
    }

    protected function addFilters(QueryBuilder &$q, $filters)
    {
        $flagOrderBy = false;
        $refl = new \ReflectionClass(new $this->_entityName());
        $docReader = new AnnotationReader();
        foreach ($filters as $filterkey => $filtervalue) {
            if ($filterkey == 'typeRequest'){
                $function = $filtervalue;
            } else{
                $function = 'andWhere';
            }
            break;
        }
        $deepOrder = false;
        $tabJoins = [];
        $tabJoinsKeys = [[]];
//        $i = 0;
        foreach ($filters as $filterkey => $filtervalues) {
            if ($filterkey == 'typeRequest'){
                continue;
            }

            if ($filterkey == 'innerJoins'){
                $filtervalues = substr($filtervalues, 1);
                $filtervalues = substr($filtervalues, 0, -1);
                $arrayJoins = explode(',',$filtervalues);
                foreach ($arrayJoins as $join) {
                    $arrayJoin = explode('=>',$join);
                    $parent = explode('@',$arrayJoin[0]);
                    $parentTable = ('App\\Entity\\Societes\\'.$parent[0] != $this->getClassName()) ? $parent[0] : $this->_alias;
                    $parentKey = $parent[1];
                    $child = explode('@',$arrayJoin[1]);
                    $childTable = $child[0];
                    $childKeys = $child[1];
                    $arrayChildKeys = explode(';',$childKeys);
                    $childId = $arrayChildKeys[0];
//                    var_dump($parentTable);
//                    var_dump($parentKey);
//                    var_dump($childTable);
//                    var_dump($childId);
//                    var_dump($childKey); exit;
                    $q->innerJoin('App:Societes\\'.$childTable, $childTable,'WITH',$parentTable.'.'.$parentKey.' = '.$childTable.'.'.$childId);
//                    $q->groupBy($childId) ;
                    $childKeysLibelle = $arrayChildKeys[1];
                    $arrayChildKeysLibelle = explode('+',$childKeysLibelle);
                    foreach ($arrayChildKeysLibelle as $childKeyLibelle){
                        $arrayChildKeyLibelle = explode('=',$childKeyLibelle);
                        $childKey = $arrayChildKeyLibelle[0];
                        $childLibelle = $arrayChildKeyLibelle[1];

                        $q->addselect("$childTable.$childKey");

                    }

                }
                continue;
            }

            $classSeparatorPos = strpos($filterkey, '@');
            $alias = $this->_alias;
            $flagDependency = false;
            $childClass = false;
            if ($classSeparatorPos !== false) {
                $flagDependency = true;
                $arrayFilter = explode('@',$filterkey);
                $currentClassName = str_replace('App\\Entity\\Societes\\', '', $this->getClassName());
                $filterkey = $arrayFilter[0];
                $childClass = $arrayFilter[1];
                $joinClassPos = array_search($childClass,$tabJoins);

                $alias = ('App\\Entity\\Societes\\'.$childClass != $this->getClassName()) ? $childClass : $this->_alias;

            }
            $arrayValues = explode('||',$filtervalues);
            $whereFunction = $function;
            $iteration = 0;
            $flagOrderBy = false;
            foreach($arrayValues as $filtervalue){

                if (($filterkey == 'order' || $filterkey == 'orderBy') && !$flagOrderBy) {
//                    if(!$flagOrderBy){
//                        $q->addOrderBy($alias.'.'.$filters['orderBy'],$filters['order']);
//                        $q->addGroupBy($alias.'.'.$filters['orderBy']);

//                    }

                    // ?orderBy=groupe+DESC+fonction+ASC+abrev
                    // ==
                    // ?orderBy=groupe DESC fonction ASC abrev
                    $orders = explode(' ', $filtervalue);
                    for ($i = 0; $i < count($orders); $i += 2) {
                        $sort = $orders[$i];
                        $order = isset($orders[$i + 1]) ? $orders[$i + 1] : 'ASC'; // si impair, dernier order ascendant
                        $sortTab = explode('@',$sort);
                        $sort = (isset($sortTab[1])) ? "$sortTab[1].$sortTab[0]" : "{$alias}.$sortTab[0]";
                        $q->addOrderBy("$sort", $order);
                    }
                    $flagOrderBy = true;
//                    $q->addOrderBy($alias.'.'.$filters['orderBy'] ,$filters['order']);
                    unset($i, $sort, $order, $orders);
//                    var_dump($q->getQuery()->getSQL()); exit;
                } elseif ($filterkey == 'deep' || $filterkey == 'deepEntity' || $filterkey == 'deepOrder') {

                    if (!$deepOrder) { // do deepOrder

                        $deepAlias = substr($filters['deepEntity'], 0, 1) . '0'; // 0 pour éviter les conflits d'alias avec l'entité principale si même alias
                        $q->join("{$alias}.{$filters['deepEntity']}", $deepAlias);

                        $orders = explode(' ', $filters['deepOrder']);
                        for ($i = 0; $i < count($orders); $i += 2) {
                            $sort = $orders[$i];
                            $order = isset($orders[$i + 1]) ? $orders[$i + 1] : null; // si impair, alors dernier order = null = 'ASC' plus tard dans doctrine

                            $q->addOrderBy("$deepAlias.$sort", $order);
                        }

                        $deepOrder = true; // don't redo it
                    }
                } else {

//              Filtres de dates
                    if (strstr($filtervalue, 'periodFilter') || strstr($filtervalue, 'sinceFilter') || strstr($filtervalue, 'untilFilter') || strstr($filtervalue, 'dateFilter')) {
                        $params = explode(';', $filtervalue);

                        switch ($params[0]) {
                            case 'periodFilter':
                                $dateDebut = (new \DateTime($params[1]))->format('Y-m-d H:i:s');
                                $dateFin = (new \DateTime($params[2]))->format('Y-m-d H:i:s');
                                $q->$whereFunction("{$alias}.$filterkey > :dateDebut".$iteration);
                                $q->$whereFunction("{$alias}.$filterkey < :dateFin".$iteration);
                                $q->setParameter('dateDebut'.$iteration, $dateDebut);
                                $q->setParameter('dateFin'.$iteration, $dateFin);
                                break;
                            case 'sinceFilter':
                                $date = (new \DateTime($params[1]))->format('Y-m-d H:i:s');
                                $q->$whereFunction("{$alias}.$filterkey > :date".$iteration);
                                $q->setParameter('date'.$iteration, $date);
                                break;
                            case 'untilFilter':
                                $date = (new \DateTime($params[1]))->format('Y-m-d H:i:s');
                                $q->$whereFunction("{$alias}.$filterkey < :date".$iteration);
                                $q->setParameter('date'.$iteration, $date);
                                break;
                            case 'dateFilter':
                                $date = (new \DateTime($params[1]))->format('Y-m-d H:i:s');
                                $q->$whereFunction("{$alias}.$filterkey = :date".$iteration);
                                $q->setParameter('date'.$iteration, $date);
                                break;
                        }
                    } else {
                        $fieldType = '';
                        $columnType = '';
                        try {
                            $not = false;
                            if ('!' === substr($filterkey, 0, 1)) {
                                $filterkey = substr($filterkey, 1);
                                $not = true;
                            }
//                            var_dump($childClass);
//                            var_dump($flagDependency);
//                            exit;
                            if (!$flagDependency && !$childClass) {
                                $docInfos = $docReader->getPropertyAnnotations($refl->getProperty($filterkey));
                                $docInfos = array_values(array_filter($docInfos, function ($annotation) {
                                    return $annotation instanceof Column;
                                }));
                                $column = $docInfos[0];
                                $columnType = $column->type;
                            }else{
                                $childClassWidthPath = '\\App\\Entity\\Societes\\'.$childClass;
                                $reflProperty = new \ReflectionProperty(new $childClassWidthPath(),$filterkey);
                                $docInfos = $reflProperty->getDocComment();
                                $searchedAnnotation = 'type="';
                                preg_match_all('#'.$searchedAnnotation.'(.*?)"#', $docInfos, $annotations);
                                $fieldType = $annotations[1][0];
                            }

                        } catch (\Exception $e) {
                            $docInfos = array();
                        }

                        if(substr($filtervalue, 0,1) == '=') {
                            $filtervalue = substr($filtervalue, 1);
                            $q->$whereFunction("{$alias}.$filterkey = '$filtervalue'");
                            $whereFunction = 'orWhere';
                            $iteration++;
                            continue;
                        }

                        if (!empty($docInfos)) {
                            if ($columnType == 'string' || $fieldType == 'string') {
                                if ($not) {
                                    $q->$whereFunction("{$alias}.$filterkey NOT LIKE :$filterkey".$iteration);
                                } else {
                                    $q->$whereFunction("{$alias}.$filterkey LIKE :$filterkey".$iteration);
                                }
//                                var_dump($filtervalue);exit;
                                $q->setParameter($filterkey.$iteration, "%{$filtervalue}%");

                            } else {
                                if ($not) {
                                    $q->$whereFunction($q->expr()->neq("{$alias}.$filterkey", $q->expr()->literal($filtervalue)));
                                } else {
                                    $q->$whereFunction("{$alias}.$filterkey = $filtervalue");
                                }
                            }
                        } else {
                            if ($not) {
                                $q->$whereFunction($q->expr()->neq("{$alias}.$filterkey", $q->expr()->literal($filtervalue)));
                            } else {
                                $q->$whereFunction("{$alias}.$filterkey = ".$q->expr()->literal($filtervalue));
                            }
//                    $q->andHaving("$filterkey = '$filtervalue'");

                        }

                    }
                }
                $whereFunction = 'orWhere';
                $iteration++;
            }

//            $i++;
        }

    }


    protected function elaboratedFilter(QueryBuilder &$q, $filter)
    {
        $arr = json_decode($filter, true);

        foreach ($arr as $key => $value) {
            if (!is_array($value)) {
                $q->andWhere("{$this->_alias}.$key = '$value'");
            } else {
                $q->andWhere($this->convertURLOperators($q, $key, $key, $value));
            }
        }
    }

    /**
     * Conversion des opérateurs dans la paire clé/valeur en paramètre
     * Cette fonction se rappelle elle-même si elle trouve un opérateur en clé (que la valeur est en tableau en fait)
     *
     * @param QueryBuilder $q
     * @param string $key
     * @param string $lastKey
     * @param mixed|array $value
     * @return Query\Expr\Comparison|mixed|null
     */
    protected function convertURLOperators(QueryBuilder &$q, $key, $lastKey, $value)
    {
        $andOr = [
            '$or' => 'orX',
            '$and' => 'andX'
        ];

        $logic = [
            '$not' => 'not',
            '$in' => 'in',
            '$nin' => 'notIn',
            '$regex' => 'like',
            '$gt' => 'gt',
            '$gte' => 'gte',
            '$lt' => 'lt',
            '$lte' => 'lte',
            '$bt' => 'between',
        ];

        // si l'opérateur est un and ou un or, on procède à un andX ou un orX grâce au QueryBuilder de Symfony
        if (array_key_exists($key, $andOr)) {
            $function = $andOr[$key]; // récup du nom de la fonction à appeler
            $elements = [];

            foreach ($value as $andOrX) { // pour chaque élément du tableau qui composera l'ensemble X=1 AND/OR Y=2 AND/OR Z=3

                if (is_array($andOrX)) {
                    foreach ($andOrX as $fieldName => $fieldValue) {
                        if (is_array($fieldValue)) {
                            foreach ($fieldValue as $cond => $v) {
                                $elements[] = $this->convertURLOperators($q, $cond, $fieldName, $v);
                            }
                        } else { // si fieldValue n'est pas un tableau, on suggère qu'il n'y a pas d'opérateur, et donc simple égalité
                            $elements[] = $q->expr()->eq("{$this->_alias}.$fieldName", "'$fieldValue'");
                        }
                    }
                } else { // si un élément du AND/OR n'est pas un tableau, on suggère qu'il n'y a pas d'opérateur, et donc simple égalité
                    $elements[] = $q->expr()->eq("{$this->_alias}.$key", "'$andOrX'");
                }
            }

            // On appelle la fonction concernée des expressions de QueryBuilder
            // On ne peut pas appeler la fonction directement car le nombre d'éléments est inconnu --> call_user_func_array
            return call_user_func_array(array($q->expr(), $function), $elements);

        } elseif (array_key_exists($key, $logic)) {
            $function = $logic[$key]; // récup du nom de la fonction d'opérateur "simple" (tout sauf andX et orX)

            // Utilisation du $lastKey au lieu de $key puisque $key est un opérateur logique
            // cf. foreach lignes 179-187

            if ($function == 'like') { // cas particulier like, mettre des %
                return $q->expr()->$function("{$this->_alias}.$lastKey", "'%$value%'");
            } elseif ($function == 'between') { // cas particulier between, value doit etre un tableau de deux valeurs
                list ($x, $y) = $value;
                return $q->expr()->$function("{$this->_alias}.$lastKey", $x, $y);
            } else {
                if (is_array($value)) { // fonctions in et notIn
                    return $q->expr()->$function("{$this->_alias}.$lastKey", $value);
                } else { // reste
                    return $q->expr()->$function("{$this->_alias}.$lastKey", "'$value'");
                }
            }
        } else {
            if (is_array($value) && !empty($value)) {
                foreach ($value as $k => $item) {
                    $function = $logic[$k];
                    return $q->expr()->$function("{$this->_alias}.$key", $item);
                }
                return null; //should never go there
            } else {
                return $q->expr()->eq("{$this->_alias}.$key", "'$value'");
            }
        }
    }
}