<?php

namespace App\Repository;

use App\Entity\Group;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class GroupRepository extends BaseRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Group::class, 'g');
    }

    public function search(array $criteria)
    {
        $q = $this->createQueryBuilder('g');

        foreach ($criteria as $field => $value) {
            if (is_string($value) and false === date_create($value)) {
                $q->orWhere($q->expr()->like("g.$field", $q->expr()->literal("%$value%")));
            } else {
                $q->orWhere($q->expr()->eq("g.$field", $value));
            }
        }
        return $q->getQuery()->getResult();
    }
}
