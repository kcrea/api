<?php

namespace App\Listener;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class LocaleListener
{
    private $container;
    private $logger;

    public function __construct(LoggerInterface $logger, ContainerInterface $container)
    {
        $this->logger = $logger;
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $this->logger->info('onKernelRequest from LocaleListener');

        $requestedLocale = $this->extractLocale($event->getRequest());
        $event->getRequest()->setLocale($requestedLocale);

        return;
    }

    private function extractLocale(Request $request)
    {
        if (null === $locale = $request->headers->get('X-Accept-Locale', null)) {
            if (null !== $acceptHeader = $request->headers->get('Accept', null)) {

                if ('*/*' === $acceptHeader) {
                    return $this->container->getParameter('locale');
                }

                $acceptHeader = explode(';', $acceptHeader);

                foreach ($acceptHeader as $oneAccept) {
                    if ('l=' === substr($oneAccept, 0, $length = 2) || 'locale=' === substr($oneAccept, 0, $length = 7)) {
                        $locale = substr($oneAccept, $length);
                    }
                }
            }
        }

        if (is_null($locale)) {
            $locale = $this->container->getParameter('locale');
        }

        return $locale;
    }
}