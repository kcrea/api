<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 19/06/2018
 * Time: 21:06
 */

namespace App\Form;


use App\Entity\Movie;
use App\Entity\MovieReview;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MovieReviewType extends ApiAbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => User::class
            ])
            ->add('movie', EntityType::class, [
                'class' => Movie::class
            ])
            ->add('content')
            ->add('rating');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => MovieReview::class
        ));
    }
}