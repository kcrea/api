<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 09/04/2018
 * Time: 10:17
 */

namespace App\Form;


use App\Entity\Group;
use App\Entity\Library;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LibraryType extends ApiAbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'data_class' => User::class,
                'required' => false
            ])
            ->add('group', EntityType::class, [
                'data_class' => Group::class,
                'required' => false
            ])
            ->add('public', CheckboxType::class, [
                'empty_data' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => Library::class
        ));
    }

}