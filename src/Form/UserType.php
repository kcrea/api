<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 09/04/2018
 * Time: 09:46
 */

namespace App\Form;


use App\Entity\User;
use App\Manager\UserManager;
use App\Service\Utils;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends ApiAbstractType
{
    private $utils;

    public function __construct(Utils $utils)
    {
        $this->utils = $utils;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('firstname')
            ->add('lastname')
            ->add('apikey', TextType::class, [
                'empty_data' => $this->utils->generateApiKey()
            ])
        ->add('avatarLink')
//        ->add('creationDate', DateTimeType::class, [
//            'disabled' => true,
//            'data' => new \DateTime()
//        ])
        ->add('email')
        ->add('password', PasswordType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => User::class
        ));
    }
}