<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 19/06/2018
 * Time: 21:06
 */

namespace App\Form;


use App\Entity\Movie;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MovieType extends ApiAbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('imdbId', TextType::class, [
                'required' => true
            ])
            ->add('releaseDate', DateTimeType::class, [
                'widget' => 'single_text'
            ])
            ->add('writer')
            ->add('posterLink')
            ->add('originalTitle')
            ->add('nationality')
            ->add('genre')
            ->add('actors');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => Movie::class
        ));
    }
}