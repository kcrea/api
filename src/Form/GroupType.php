<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 09/04/2018
 * Time: 09:46
 */

namespace App\Form;


use App\Entity\Group;
use App\Entity\User;
use App\Manager\UserManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupType extends ApiAbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label', TextType::class)
//            ->add('creationDate', DateTimeType::class, [
//                'empty_data' => new \DateTime()
//            ])
//            ->add('creator', EntityType::class, [
//                'class' => User::class
//            ])
            ->add('avatarLink', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => Group::class,
        ));
    }
}