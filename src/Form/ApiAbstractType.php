<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 24/04/2018
 * Time: 23:23
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class ApiAbstractType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'csrf_protection' => false
        ]);
    }
}