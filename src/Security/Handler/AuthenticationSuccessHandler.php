<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 22/08/2017
 * Time: 11:33
 */

namespace App\Security\Handler;


use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class AuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $serializationContext = SerializationContext::create()->enableMaxDepthChecks()->setSerializeNull(true);

        /** @var User $user */
        $user = $token->getUser();

        $serializedToken = $this->serializer->serialize($user, 'json', $serializationContext->setGroups(['User']));

        $response = new Response($serializedToken, Response::HTTP_OK);

        return $response;
    }

}