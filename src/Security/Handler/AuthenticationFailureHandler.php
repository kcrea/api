<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 22/08/2017
 * Time: 11:40
 */

namespace App\Security\Handler;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

class AuthenticationFailureHandler implements AuthenticationFailureHandlerInterface
{
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        switch ($exception->getMessage()) {
            case 'Bad credentials.':
                $code = 401;
                break;
            default:
                $code = 500;
        }
        return new JsonResponse(['code' => $code, 'message' => $exception->getMessage()], $code);
    }
}