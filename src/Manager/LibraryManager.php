<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 28/05/2018
 * Time: 17:49
 */

namespace App\Manager;


use App\Entity\Group;
use App\Entity\Library;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;

class LibraryManager extends AbstractManager
{
    public function __construct(
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        TranslatorInterface $translator,
        TokenStorageInterface $tokenStorage
    ) {
        parent::__construct($entityManager, $formFactory, $translator, $tokenStorage);
    }

    /**
     * @return array
     */
    public function findPublic()
    {
        return $this->getRepository()->findBy([
            'public' => true
        ]);
    }

    /**
     * @return UserRepository|ObjectRepository
     */
    public function getRepository()
    {
        return parent::getEntityRepository('App:Library');
    }

    public function createLibrary($owner, $isGroup = false)
    {
        $lib = new Library();
        $lib->setPublic(false);
        if ($isGroup) {
            $lib->setGroup($owner);
        } else {
            $lib->setUser($owner);
        }
        $this->merge($lib, true);
        return $lib;
    }

    public function canEdit(Library $library, User $user)
    {
        if (null === $library) {
            throw new NotFoundHttpException($this->translator->trans('error.http.404'));
        }
        if (null !== $library->getUser()) {
            if (!$user->isEqualTo($library->getUser())) {
                throw new AccessDeniedHttpException($this->translator->trans('error.library.cant_edit'));
            }
        } elseif (null !== $library->getGroup()) {
            if (!$this->isUserInGroup($library->getGroup(), $user)) {
                throw new AccessDeniedHttpException($this->translator->trans('error.library.cant_edit'));
            }
        }
    }

    public function isUserInGroup(Group $group, User $user)
    {
        if ($group->getCreator()->isEqualTo($user)) {
            return true;
        }

        foreach ($group->getUsers() as $userGroup) {
            if ($userGroup->getUser()->isEqualTo($user)) {
                return true;
            }
        }
        return false;
    }

    public function throwNotFoundIfPrivate(Library $library)
    {
        $user = $library->getUser();
        $group = null === $user ? $library->getGroup() : null;

        if (!$library->isPublic()) {
            if (null !== $user) {
                if (null === $this->currentUser || !$user->isEqualTo($this->currentUser)) {
                    throw new NotFoundHttpException($this->translator->trans('error.http.404'));
                }
            } elseif (null !== $group) {
                if (!$this->isUserInGroup($group, $this->currentUser)) {
                    throw new NotFoundHttpException($this->translator->trans('error.http.404'));
                }
            } else {
                throw new UnprocessableEntityHttpException($this->translator->trans('error.library.not_linked'));
            }
        }
    }

}