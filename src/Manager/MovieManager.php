<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 28/05/2018
 * Time: 17:49
 */

namespace App\Manager;


use App\Entity\Movie;
use App\Form\MovieType;
use App\Repository\MovieRepository;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;

class MovieManager extends AbstractManager
{
    public function __construct(
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        TranslatorInterface $translator,
        TokenStorageInterface $tokenStorage
    )
    {
        parent::__construct($entityManager, $formFactory, $translator, $tokenStorage);
    }

    public function findMovie($id)
    {
        if (is_numeric($id)) {
            $element = $this->getRepository()->find($id);

            if (null === $element) {
                throw new NotFoundHttpException($this->translator->trans('error.http.404'));
            }
            return $element;
        } else {
            $element = $this->getRepository()->findOneBy([
                'imdb_id' => $id
            ]);

            if (null === $element) {
                throw new NotFoundHttpException($this->translator->trans('error.http.404'));
            }
            return $element;
        }
    }

    /**
     * @return MovieRepository|ObjectRepository
     */
    public function getRepository()
    {
        return parent::getEntityRepository('App:Movie');
    }

    public function submitFormMovie(Request $request)
    {
        $movie = new Movie();
        $submittedData = json_decode($request->getContent(), true);

        $form = $this->formF->create(MovieType::class, $movie);
        $form->submit($submittedData, false);
        if ($form->isValid()) {
            $this->persist($movie);
            $this->flush();
        } else {
            return [
                'formError' => true,
                'form' => $form->getErrors()
            ];
        }
        return $movie;

    }
}