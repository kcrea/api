<?php
/**
 * Created by PhpStorm.
 * Group: rvale
 * Date: 09/04/2018
 * Time: 07:54
 */

namespace App\Manager;

use App\Entity\Group;
use App\Entity\User;
use App\Form\GroupType;
use App\Repository\GroupRepository;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;

class GroupManager extends AbstractManager
{
    public function __construct(
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        TranslatorInterface $translator,
        TokenStorageInterface $tokenStorage
    )
    {
        parent::__construct($entityManager, $formFactory, $translator, $tokenStorage);
    }

    public function search(array $criteria)
    {
        return $this->getRepository()->search($criteria);
    }

    /**
     * @param string $repo
     * @return GroupRepository|ObjectRepository
     */
    public function getRepository($repo = 'App:Group')
    {
        return parent::getEntityRepository($repo);
    }

    public function removeGroup(Request $request, $id, User $currentUser)
    {
        $group = $this->findGroup($id, $currentUser, true);

        $this->remove($group, true);
    }

    public function createGroup(Request $request, User $currentUser)
    {
        $group = new Group();

        return $this->submitFormGroup($request, $group, $currentUser);
    }

    public function updateGroup(Request $request, $id, User $currentUser)
    {
        $group = $this->findGroup($id, $currentUser, true);

        return $this->submitFormGroup($request, $group, $currentUser);
    }

    public function submitFormGroup(Request $request, Group $group, User $currentUser)
    {
        $form = $this->formF->create(GroupType::class, $group);
        $form->submit(json_decode($request->getContent(), true), false);
        if ($form->isValid()) {
            $group->setCreator($currentUser);
            $this->persist($group);
            $this->flush();
        } else {
            return [
                'formError' => true,
                'form' => $form->getErrors()
            ];
        }
        return $group;
    }

    /**
     * @param Group $entity
     * @param User $currentUser
     *
     * @throws NotFoundHttpException, AccessDeniedHttpException If the user does not exist or is not the one who launched the request
     */
    private function canEdit(Group $entity, User $currentUser)
    {
        if (null === $entity) {
            throw new NotFoundHttpException($this->translator->trans('error.http.404'));
        }

        $mustThrow = true;
        if (!$entity->getCreator()->isEqualTo($currentUser)) {
            foreach ($entity->getUsers() as $userGroup) {
                if ($userGroup->getUser()->isEqualTo($currentUser)) {
                    $mustThrow = false;
                    break;
                }
            }
        } else {
            $mustThrow = false;
        }

        if ($mustThrow) {
            throw new AccessDeniedHttpException($this->translator->trans('error.user.not_admin_group'));
        }
    }

    /**
     * @param int $id
     * @param User $currentUser
     * @param bool $tryCanEdit
     * @return Group
     */
    public function findGroup($id, User $currentUser, $tryCanEdit = false)
    {
        /** @var Group $entity */
        $entity = $this->getRepository()->find($id);

        if (null === $entity) {
            throw new NotFoundHttpException($this->translator->trans('error.http.404'));
        }

        if ($tryCanEdit) {
            $this->canEdit($entity, $currentUser);
        }
        return $entity;
    }

    public function findUserGroup(Group $group, $id)
    {
        return $this->getRepository('App:UserGroup')->findOneBy([
            'group' => $group,
            'user' => $id
        ]);
    }

    /**
     * @param Group $group
     * @param User $user
     */
    public function throwIfCreator(Group $group, User $user) {
        if ($group->getCreator()->isEqualTo($user)) {
            throw new AccessDeniedHttpException($this->translator->trans("error.group.cant_expel_creator"));
        }
    }
}