<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 09/04/2018
 * Time: 07:54
 */

namespace App\Manager;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class UserManager extends AbstractManager
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        UserPasswordEncoderInterface $encoder,
        TranslatorInterface $translator,
        TokenStorageInterface $tokenStorage
    )
    {
        parent::__construct($entityManager, $formFactory, $translator, $tokenStorage);
        $this->encoder = $encoder;
    }

    /**
     * @return UserRepository|ObjectRepository
     */
    public function getRepository()
    {
        return parent::getEntityRepository('App:User');
    }

    public function authenticate($username)
    {
        return $this->getRepository()->findOneByUsernameOrEmail($username);
    }

    public function removeUser(Request $request, $id, User $currentUser)
    {
        $user = $this->findUser($id, $currentUser, true);

        $this->remove($user, true);
    }

    public function createUser(Request $request)
    {
        $user = new User();

        return $this->submitFormUser($request, $user);
    }

    public function updateUser(Request $request, $id, User $currentUser)
    {
        $user = $this->findUser($id, $currentUser, true);

        return $this->submitFormUser($request, $user);
    }

    public function submitFormUser(Request $request, User $user)
    {
        $submittedData = json_decode($request->getContent(), true);

        if (in_array('apikey', array_keys($submittedData))) {
            throw new BadRequestHttpException($this->translator->trans('error.user.api_key'));
        }

        if ('POST' === $request->getMethod()) {
            $submittedData['apikey'] = $this->generateApiKey();
        } else {
            if (in_array('password', array_keys($submittedData))) {
                throw new BadRequestHttpException($this->translator->trans('error.user.password'));
            }
        }

        $form = $this->formF->create(UserType::class, $user);
        $form->submit($submittedData, false);
        if ($form->isValid()) {
            $this->persist($user);
            $this->flush();
        } else {
            return [
                'formError' => true,
                'form' => $form->getErrors()
            ];
        }
        return $user;
    }

    public function updatePasswordUser(Request $request, int $id, User $currentUser)
    {
        $user = $this->findUser($id, $currentUser, true);

        if (!$this->encoder->isPasswordValid($user, $request->request->get('current_password'))) {
            throw new BadRequestHttpException($this->translator->trans('error.user.change_pass.wrong_current'));
        }

        if ($request->request->get('password') !== $request->request->get('confirm_password')) {
            throw new BadRequestHttpException($this->translator->trans('error.user.change_pass.mismatch'));
        }

        $user->setPassword($this->encoder->encodePassword($user, $request->request->get('password')));
        $this->persist($user, true);

        return $user;
    }

    /**
     * @param User $entity
     * @param User $currentUser
     * @param bool $selfOnly
     *
     * @throws NotFoundHttpException, AccessDeniedHttpException If the user does not exist or is not the one who launched the request
     */
    private function canEdit(User $entity, User $currentUser, $selfOnly = true)
    {
        if (null === $entity) {
            throw new NotFoundHttpException($this->translator->trans('error.http.404'));
        }
        if (!$entity->isEqualTo($currentUser) && $selfOnly) {
            throw new AccessDeniedHttpException($this->translator->trans('error.user.not_self'));
        }
    }

    /**
     * @param string|int $id
     * @param User $currentUser
     * @param bool $tryCanEdit
     * @return User
     */
    private function findUser($id, User $currentUser, $tryCanEdit = false)
    {
        if ('me' === $id) {
            return $currentUser;
        }
        /** @var User $entity */
        $entity = $this->getRepository()->find($id);

        if (null === $entity) {
            throw new NotFoundHttpException($this->translator->trans('error.http.404'));
        }

        if ($tryCanEdit) {
            $this->canEdit($entity, $currentUser);
        }
        return $entity;
    }

    private function generateApiKey()
    {
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"

        return substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4) . $hyphen
            . substr($charid, 12, 4) . $hyphen
            . substr($charid, 16, 4) . $hyphen
            . substr($charid, 20, 12);
    }
}