<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 09/04/2018
 * Time: 07:54
 */

namespace App\Manager;


use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;

abstract class AbstractManager
{
    private $em;
    protected $translator;
    protected $formF;
    protected $currentUser;

    public function __construct(
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        TranslatorInterface $translator,
        TokenStorageInterface $tokenStorage
    )
    {
        $this->em = $entityManager;
        $this->formF = $formFactory;
        $this->translator = $translator;
        if (null === $tokenStorage->getToken() || !is_object($tokenStorage->getToken()->getUser())) {
            // e.g. No token or anonymous
            $this->currentUser = null;
        } else {
            $this->currentUser = $tokenStorage->getToken()->getUser();
        }
    }

    protected function getEntityRepository($className)
    {
        return $this->em->getRepository($className);
    }

    public function persist($entity, $andFlush = false)
    {
        $this->em->persist($entity);
        if ($andFlush) {
            $this->flush();
        }
    }

    public function merge($entity, $andFlush = false)
    {
        $this->em->merge($entity);
        if ($andFlush) {
            $this->flush();
        }
    }

    public function remove($entity, $andFlush = false)
    {
        $this->em->remove($entity);
        if ($andFlush) {
            $this->flush();
        }
    }

    public function flush()
    {
        $this->em->flush();
    }

}