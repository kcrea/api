<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 09/04/2018
 * Time: 08:42
 */

namespace App\Command;


use App\Manager\UserManager;
use App\Service\Utils;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateApiKeyCommand extends Command
{
    private $utils;

    public function __construct(Utils $utils)
    {
        $this->utils = $utils;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:gen-api-key')

            // the short description shown while running "php bin/console list"
            ->setDescription('Generates an Api Key for a user')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to generate an api key.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            $this->utils->generateApiKey(),
            ''
        ]);
    }
}