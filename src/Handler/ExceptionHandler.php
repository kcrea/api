<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 17/01/2018
 * Time: 14:41
 */

namespace App\Handler;


use FOS\RestBundle\Serializer\Normalizer\AbstractExceptionNormalizer;
use FOS\RestBundle\Util\ExceptionValueMap;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use PHPUnit\Exception;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Response;

class ExceptionHandler implements SubscribingHandlerInterface
{
    /**
     * @var ExceptionValueMap
     */
    private $messagesMap;

    /**
     * @var bool
     */
    private $debug;

    /**
     * Return format:
     *
     *      array(
     *          array(
     *              'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
     *              'format' => 'json',
     *              'type' => 'DateTime',
     *              'method' => 'serializeDateTimeToJson',
     *          ),
     *      )
     *
     * The direction and method keys can be omitted.
     *
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => \Exception::class,
                'method' => 'serializeExceptionToJson',
            ],
//            [
//                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
//                'format' => 'json',
//                'type' => FlattenException::class,
//                'method' => 'serializeFlattenExceptionToJson'
//            ]
        ];
    }

    /**
     * @param ExceptionValueMap $messagesMap
     * @param bool  $debug
     */
    public function __construct(ExceptionValueMap $messagesMap, $debug)
    {
        $this->messagesMap = $messagesMap;
        $this->debug = $debug;
    }

    public function serializeExceptionToJson(
        JsonSerializationVisitor $visitor,
        \Exception $exception,
        array $type,
        Context $context
    ) {
        $data = $this->convertToArray($exception, $context);

        return $visitor->visitArray($data, $type, $context);
    }

//    public function serializeFlattenExceptionToJson(
//        JsonSerializationVisitor $visitor,
//        FlattenException $exception,
//        array $type,
//        Context $context
//    )
//    {
//        $data = $this->convertToArray($exception, $context);
//
//        return $visitor->visitArray($data, $type, $context);
//    }

    /**
     * @param \Exception $exception
     *
     * @return array
     */
    private function convertToArray(\Exception $exception, Context $context)
    {
        $data = [];

        $templateData = $context->attributes->get('template_data');
        if ($templateData->isDefined()) {
            $data['code'] = $statusCode = $templateData->get()['status_code'];
        }

        $data['message'] = $this->getExceptionMessage($exception, isset($statusCode) ? $statusCode : null);

        return $data;
    }

    /**
     * Extracts the exception message.
     *
     * @param \Exception $exception
     * @param int|null   $statusCode
     *
     * @return string
     */
    private function getExceptionMessage($exception, $statusCode = null)
    {
        $showMessage = $this->messagesMap->resolveException($exception);

        if ($showMessage || $this->debug) {
            return $exception->getMessage();
        }

        return array_key_exists($statusCode, Response::$statusTexts) ? Response::$statusTexts[$statusCode] : 'error';
    }
}