<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 18/01/2018
 * Time: 14:01
 */

namespace App\Handler;

use FOS\RestBundle\Serializer\Serializer;
use FOS\RestBundle\Util\ExceptionValueMap;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler as FOSViewHandler;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;

class ViewHandler extends FOSViewHandler
{
    private $reqStack;
    private $translator;

    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        Serializer $serializer,
        EngineInterface $templating = null,
        RequestStack $requestStack,
        array $formats = null,
        $failedValidationCode = Response::HTTP_BAD_REQUEST,
        $emptyContentCode = Response::HTTP_NO_CONTENT,
        $serializeNull = false,
        array $forceRedirects = null,
        $defaultEngine = 'twig',
        TranslatorInterface $translator
    )
    {
        $this->translator = $translator;
        $this->reqStack = $requestStack;
        parent::__construct($urlGenerator, $serializer, $templating, $requestStack);
    }

    /**
     * Handles a request with the proper handler.
     *
     * Decides on which handler to use based on the request format.
     *
     * @param View $view
     * @param Request $request
     *
     * @throws UnsupportedMediaTypeHttpException
     *
     * @return Response
     */
    public function handle(View $view, Request $request = null)
    {
        if (null === $request) {
            $request = $this->reqStack->getCurrentRequest();
        }

        $format = $view->getFormat() ?: $request->getRequestFormat();

        if (!$this->supports($format)) {
            throw new UnsupportedMediaTypeHttpException($this->translator->trans('error.unsupported_media_type', [
                '%format%' => $format
            ]));
        }

        if (isset($this->customHandlers[$format])) {
            return call_user_func($this->customHandlers[$format], $this, $view, $request, $format);
        }

        return $this->createResponse($view, $request, $format);
    }
}