<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 18/01/2018
 * Time: 14:46
 */

namespace App\Handler;


use FOS\RestBundle\Util\ExceptionValueMap;
use FOS\RestBundle\View\ConfigurableViewHandlerInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExceptionViewHandler
{
    public function createResponse(ConfigurableViewHandlerInterface $handler, View $view, Request $request)
    {
        if (!$view->getData() instanceof \Exception) {
            throw new \LogicException(sprintf('La méthode %s::%s ne devrait gérer que des Exceptions', self::class, 'createResponse'));
        }

        /** @var HttpException $exc */
        if (($exc = $view->getData()) instanceof HttpException) {
            $content = [
                'code' => $code = $exc->getStatusCode(),
                'message' => $exc->getMessage()
            ];
        } else {
            $content = [
                'code' => $code = Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $view->getData()->getMessage()
            ];
        }
        return new JsonResponse($content, $code, $view->getHeaders());
    }
}