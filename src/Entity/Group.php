<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="`group`")
 * @ORM\Entity(repositoryClass="App\Repository\GroupRepository")
 */
class Group
{
    public function __construct()
    {
        $this->creation_date = new \DateTime();
        $this->users = new ArrayCollection();
    }

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"Group", "User"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=150)
     *
     * @Serializer\Groups({"Group", "User"})
     */
    private $label;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     *
     * @Serializer\Groups({"Group"})
     */
    private $avatar_link;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     *
     * @Serializer\Groups({"Group"})
     */
    private $creation_date;

    /**
     * @var Library
     *
     * @ORM\OneToOne(targetEntity="Library", inversedBy="group", cascade={"all"})
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="id_library", referencedColumnName="id")
     * )
     *
     * @Serializer\Groups({"Group", "User"})
     */
    private $library;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="groups_created")
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="id_creator", referencedColumnName="id")
     * )
     *
     * @Serializer\Groups({"Group", "User"})
     */
    private $creator;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserGroup", mappedBy="group", cascade={"all"})
     *
     * @Serializer\Groups({"Group"})
     */
    private $users;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Group
     */
    public function setLabel(string $label): ?Group
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatarLink(): ?string
    {
        return $this->avatar_link;
    }

    /**
     * @param string $avatar_link
     * @return Group
     */
    public function setAvatarLink(string $avatar_link): ?Group
    {
        $this->avatar_link = $avatar_link;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate(): ?\DateTime
    {
        return $this->creation_date;
    }

    /**
     * @param \DateTime $creation_date
     * @return Group
     */
    public function setCreationDate(\DateTime $creation_date): ?Group
    {
        $this->creation_date = $creation_date;
        return $this;
    }

    /**
     * @return Library
     */
    public function getLibrary(): ?Library
    {
        return $this->library;
    }

    /**
     * @param Library $library
     * @return Group
     */
    public function setLibrary(Library $library): ?Group
    {
        $this->library = $library;
        return $this;
    }

    /**
     * @return User
     */
    public function getCreator(): ?User
    {
        return $this->creator;
    }

    /**
     * @param User $creator
     * @return Group
     */
    public function setCreator(User $creator): ?Group
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers(): ?ArrayCollection
    {
        return $this->users;
    }

    /**
     * @param User $user
     * @param bool $admin
     * @return Group|null
     */
    public function addUser(User $user, $admin = false): ?Group
    {
        $ug = new UserGroup();
        $ug->setGroup($this)->setUser($user)->setAdmin($admin);
        $this->users->add($ug);
        return $this;
    }

    /**
     * @param User $user
     * @return Group|null
     */
    public function removeUser(User $user): ?Group
    {
        $this->users->removeElement($user);
        return $this;
    }
}
