<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="library")
 * @ORM\Entity(repositoryClass="App\Repository\LibraryRepository")
 */
class Library
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"Library","User", "Group"})
     */
    private $id;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Serializer\Groups({"Library", "User", "Group"})
     */
    private $public;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User", mappedBy="library")
     * @Serializer\Groups({"Library"})
     */
    private $user;

    /**
     * @var Group
     *
     * @ORM\OneToOne(targetEntity="Group", mappedBy="library")
     * @Serializer\Groups({"Library"})
     */
    private $group;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Movie", cascade={"all"})
     * @ORM\JoinTable(
     *     name="movie_library",
     *     joinColumns={@ORM\JoinColumn(name="id_library", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="id_movie", referencedColumnName="id")}
     *     )
     * @Serializer\Groups({"Library"})
     */
    private $movies;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isPublic(): ?bool
    {
        return $this->public;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return Group
     */
    public function getGroup(): ?Group
    {
        return $this->group;
    }

    /**
     * @param mixed $public
     * @return Library
     */
    public function setPublic($public)
    {
        $this->public = $public;
        return $this;
    }

    /**
     * @param User $user
     * @return Library
     */
    public function setUser(User $user): ?Library
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param Group $group
     * @return Library
     */
    public function setGroup(Group $group): ?Library
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMovies(): ?ArrayCollection
    {
        return $this->movies;
    }

    /**
     * @param Movie $movie
     * @return $this
     */
    public function addMovie(Movie $movie): ?Library
    {
        $this->movies->add($movie);
        return $this;
    }

    /**
     * @param Movie $movie
     * @return $this
     */
    public function removeMovie(Movie $movie): ?Library
    {
        $this->movies->removeElement($movie);
        return $this;
    }
}
