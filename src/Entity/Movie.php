<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="movie")
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"Movie", "Library", "MovieReview"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=25, unique=true)
     * @Serializer\Groups({"Movie", "Library"})
     */
    private $imdb_id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Serializer\Groups({"Movie", "Library"})
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Serializer\Groups({"Movie", "Library"})
     */
    private $nationality;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Serializer\Groups({"Movie", "Library"})
     */
    private $original_title;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     * @Serializer\Groups({"Movie", "Library"})
     */
    private $release_date;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Groups({"Movie", "Library"})
     */
    private $writer;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Groups({"Movie", "Library"})
     */
    private $actors;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Groups({"Movie", "Library"})
     */
    private $genre;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"Movie", "Library"})
     */
    private $poster_link;

    /**
     * @var ArrayCollection|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="MovieReview", mappedBy="movie", cascade={"all"})
     */
    private $reviews;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getImdbId(): ?string
    {
        return $this->imdb_id;
    }

    /**
     * @param string $imdb_id
     * @return Movie
     */
    public function setImdbId(string $imdb_id): ?Movie
    {
        $this->imdb_id = $imdb_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Movie
     */
    public function setTitle(string $title): ?Movie
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     * @return Movie
     */
    public function setNationality(string $nationality): ?Movie
    {
        $this->nationality = $nationality;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalTitle(): ?string
    {
        return $this->original_title;
    }

    /**
     * @param string $original_title
     * @return Movie
     */
    public function setOriginalTitle(string $original_title): ?Movie
    {
        $this->original_title = $original_title;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getReleaseDate(): ?\DateTime
    {
        return $this->release_date;
    }

    /**
     * @param \DateTime $release_date
     * @return Movie
     */
    public function setReleaseDate(\DateTime $release_date): ?Movie
    {
        $this->release_date = $release_date;
        return $this;
    }

    /**
     * @return string
     */
    public function getWriter(): ?string
    {
        return $this->writer;
    }

    /**
     * @param string $writer
     * @return Movie
     */
    public function setWriter(string $writer): ?Movie
    {
        $this->writer = $writer;
        return $this;
    }

    /**
     * @return string
     */
    public function getActors(): ?string
    {
        return $this->actors;
    }

    /**
     * @param string $actors
     * @return Movie
     */
    public function setActors(string $actors): ?Movie
    {
        $this->actors = $actors;
        return $this;
    }

    /**
     * @return string
     */
    public function getGenre(): ?string
    {
        return $this->genre;
    }

    /**
     * @param string $genre
     * @return Movie
     */
    public function setGenre(string $genre): ?Movie
    {
        $this->genre = $genre;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosterLink(): ?string
    {
        return $this->poster_link;
    }

    /**
     * @param string $poster_link
     * @return Movie
     */
    public function setPosterLink(string $poster_link): ?Movie
    {
        $this->poster_link = $poster_link;
        return $this;
    }

    /**
     * @return ArrayCollection|PersistentCollection|null
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @param MovieReview $review
     * @return Movie|null
     */
    public function addReview(MovieReview $review): ?Movie
    {
        $this->reviews->add($review);
        return $this;
    }

    /**
     * @param MovieReview $review
     * @return Movie|null
     */
    public function removeReview(MovieReview $review): ?Movie
    {
        $this->reviews->removeElement($review);
        return $this;
    }
}
