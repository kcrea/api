<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="user_group")
 * @ORM\Entity(repositoryClass="App\Repository\UserGroupRepository")
 */
class UserGroup
{
    /**
     * @var User
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="groups")
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * )
     *
     * @Serializer\Groups({"Group"})
     */
    private $user;

    /**
     * @var Group
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="users")
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="id_group", referencedColumnName="id")
     * )
     *
     * @Serializer\Groups({"User"})
     */
    private $group;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     *
     * @Serializer\Groups({"User", "Group"})
     */
    private $admin;

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserGroup
     */
    public function setUser(User $user): ?UserGroup
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Group
     */
    public function getGroup(): ?Group
    {
        return $this->group;
    }

    /**
     * @param Group $group
     * @return UserGroup
     */
    public function setGroup(Group $group): ?UserGroup
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAdmin(): ?bool
    {
        return $this->admin;
    }

    /**
     * @param bool $admin
     * @return UserGroup
     */
    public function setAdmin(bool $admin): ?UserGroup
    {
        $this->admin = $admin;
        return $this;
    }
}
