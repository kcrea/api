<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="movie_review")
 * @ORM\Entity(repositoryClass="App\Repository\MovieReviewRepository")
 */
class MovieReview
{
    /**
     * @var User
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reviews")
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * )
     *
     * @Serializer\Groups({"MovieReview"})
     */
    private $user;

    /**
     * @var Movie
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="Movie", cascade={"persist"})
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="id_movie", referencedColumnName="id")
     * )
     *
     * @Serializer\Groups({"MovieReview"})
     */
    private $movie;

    /**
     * @var string
     * @ORM\Column(type="string", length=280, nullable=true)
     * @Serializer\Groups({"MovieReview"})
     */
    private $content;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Groups({"MovieReview"})
     */
    private $rating;

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return MovieReview
     */
    public function setUser(User $user): ?MovieReview
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Movie
     */
    public function getMovie(): ?Movie
    {
        return $this->movie;
    }

    /**
     * @param Movie $movie
     * @return MovieReview
     */
    public function setMovie(Movie $movie): ?MovieReview
    {
        $this->movie = $movie;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return MovieReview
     */
    public function setContent(string $content): ?MovieReview
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return int
     */
    public function getRating(): ?int
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     * @return MovieReview
     */
    public function setRating(int $rating): ?MovieReview
    {
        $this->rating = $rating;
        return $this;
    }
}
