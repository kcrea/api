<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements AdvancedUserInterface, EquatableInterface
{
    public function __construct()
    {
        $this->creation_date = new \DateTime();
    }

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"User", "Library", "MovieReview", "Group"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     *
     * @Serializer\Groups({"User"})
     */
    private $firstname;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     *
     * @Serializer\Groups({"User"})
     */
    private $lastname;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     *
     * @Serializer\Groups({"User"})
     */
    private $avatar_link;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, unique=true)
     *
     * @Serializer\Groups({"User", "Library"})
     */
    private $username;

    /**
     * @var string
     * @ORM\Column(type="string", length=250, unique=true)
     *
     * @Serializer\Groups({"User"})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Exclude()
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     *
     * @Serializer\Groups({"User"})
     */
    private $apikey;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     *
     * @Serializer\Groups({"User"})
     */
    private $creation_date;

    /**
     * @var Library
     *
     * @ORM\OneToOne(targetEntity="Library", inversedBy="user", cascade={"all"})
     * @ORM\JoinColumns(
     *     @ORM\JoinColumn(name="id_library", referencedColumnName="id")
     * )
     *
     * @Serializer\Groups({"User"})
     */
    private $library;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Group", mappedBy="creator")
     *
     * @Serializer\Groups({"User"})
     */
    private $groups_created;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="UserGroup", mappedBy="user")
     *
     * @Serializer\Groups({"User"})
     */
    private $groups;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="MovieReview", mappedBy="user")
     *
     * @Serializer\Groups({"User"})
     */
    private $reviews;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return User
     */
    public function setFirstname(string $firstname): ?User
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return User
     */
    public function setLastname(string $lastname): ?User
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatarLink(): ?string
    {
        return $this->avatar_link;
    }

    /**
     * @param string $avatar_link
     * @return User
     */
    public function setAvatarLink(string $avatar_link): ?User
    {
        $this->avatar_link = $avatar_link;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): ?User
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): ?User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): ?User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getApikey(): ?string
    {
        return $this->apikey;
    }

    /**
     * @param string $apikey
     * @return User
     */
    public function setApikey(string $apikey): ?User
    {
        $this->apikey = $apikey;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate(): ?\DateTime
    {
        return $this->creation_date;
    }

    /**
     * @param \DateTime $creation_date
     * @return User
     */
    public function setCreationDate(\DateTime $creation_date): ?User
    {
        $this->creation_date = $creation_date;
        return $this;
    }

    /**
     * @return Library
     */
    public function getLibrary(): ?Library
    {
        return $this->library;
    }

    /**
     * @param Library $library
     * @return User
     */
    public function setLibrary(Library $library): ?User
    {
        $this->library = $library;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getGroupsCreated(): ?ArrayCollection
    {
        return $this->groups_created;
    }

    /**
     * @return ArrayCollection
     */
    public function getReviews(): ?ArrayCollection
    {
        return $this->reviews;
    }

    /**
     * @return ArrayCollection
     */
    public function getGroups(): ?ArrayCollection
    {
        return $this->groups;
    }
    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return true;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return string[] The user roles
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * Also implementation should consider that $user instance may implement
     * the extended user interface `AdvancedUserInterface`.
     *
     * @param UserInterface $user the user to compare to
     * @return bool
     */
    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }
}
