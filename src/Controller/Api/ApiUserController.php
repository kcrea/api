<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 09/04/2018
 * Time: 09:35
 */

namespace App\Controller\Api;


use App\Entity\Library;
use App\Entity\User;
use App\Form\UserType;
use App\Manager\LibraryManager;
use App\Manager\UserManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiUserController extends ApiRestController
{
    /**
     * Récupère les informations d'un utilisateur
     *
     * @SWG\Tag(name="Utilisateurs")
     * @SWG\Response(
     *     response=200,
     *     description="Retourne un utilisateur et ses dépendances les plus importantes",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=User::class, groups={"User"})
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Un entier positif identifiant d'un utilisateur"
     * )
     *
     * @Rest\View(serializerGroups={"User"})
     * @Rest\Get("/users/{id}", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param $id
     * @return View
     */
    public function getOneUser(Request $request, $id)
    {
        return $this->view(parent::getOneEntity($request, User::class, $id), Response::HTTP_OK);
    }

    /**
     * Crée un utilisateur
     * @SWG\Tag(name="Utilisateurs")
     * @SWG\Response(
     *     response=201,
     *     description="Crée et retourne l'utilisateur",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=User::class, groups={"User"})
     *     )
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     type="json",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=UserType::class)
     *     )
     * )
     * @param Request $request
     * @param UserManager $userManager
     * @param LibraryManager $libMng
     * @return View
     *
     * @Rest\View(serializerGroups={"User"})
     * @Rest\Post("/users")
     */
    public function postCreateUser(Request $request, UserManager $userManager)
    {
        $result = $userManager->createUser($request);
        if (!(is_array($result) && isset($result['formError']))) {
//            $library = $libMng->createLibrary($result);
            $library = new Library();
            $library->setPublic(false);
            $library->setUser($result);
            $result->setLibrary($library);
            $userManager->merge($result, true);
        }
        return $this->view($result, (is_array($result) && isset($result['formError'])) ?
            Response::HTTP_BAD_REQUEST :
            Response::HTTP_CREATED);
    }

    /**
     * Met à jour un utilisateur
     * @SWG\Tag(name="Utilisateurs")
     * @SWG\Response(
     *     response=200,
     *     description="Met à jour et retourne l'utilisateur",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=User::class, groups={"User"})
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Un entier positif identifiant d'un utilisateur"
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     type="json",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=UserType::class)
     *     )
     * )
     * @param Request $request
     * @param int $id
     * @param UserManager $userManager
     * @return View
     *
     * @Rest\View(serializerGroups={"User"})
     * @Rest\Put("/users/{id}", requirements={"id"="\d+"})
     * @Rest\Patch("/users/{id}", requirements={"id"="\d+"})
     */
    public function updateUser(Request $request, int $id, UserManager $userManager)
    {
        $result = $userManager->updateUser($request, $id, $this->getUser());
        return $this->view($result, (is_array($result) && isset($result['formError'])) ? Response::HTTP_BAD_REQUEST : Response::HTTP_OK);
    }

    /**
     * Change le mot de passe d'un utilisateur
     * @SWG\Tag(name="Utilisateurs")
     * @SWG\Response(
     *     response=200,
     *     description="Met à jour le mot de passe d'un utilisateur et retourne l'utilisateur",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=User::class, groups={"User"})
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Un entier positif identifiant d'un utilisateur"
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     type="json",
     *     @SWG\Schema(
     *       type="object",
     *       @SWG\Property(type="string", property="current_password"),
     *       @SWG\Property(type="string", property="password"),
     *       @SWG\Property(type="string", property="confirm_password")
     *     )
     * )
     * @param Request $request
     * @param int $id
     * @param UserManager $userManager
     * @return View
     *
     * @Rest\View(serializerGroups={"User"})
     * @Rest\Put("/users/{id}/change-password", requirements={"id"="\d+"})
     * @Rest\Patch("/users/{id}/change-password", requirements={"id"="\d+"})
     */
    public function putChangePasswordUser(Request $request, int $id, UserManager $userManager)
    {
        return $this->view($userManager->updatePasswordUser($request, $id, $this->getUser()));
    }

    /**
     * Vérifie si la valeur d'un champ est déjà enregistrée
     * @SWG\Tag(name="Utilisateurs")
     * @SWG\Response(
     *     response=200,
     *     description="Vérifie si un champ à valeur unique est déjà pris ou non",
     *     @SWG\Schema(type="boolean")
     * )
     * @SWG\Parameter(
     *     name="field",
     *     in="path",
     *     type="string",
     *     description="Le champ à vérifier (email|username)"
     * )
     * @SWG\Parameter(
     *     name="value",
     *     in="path",
     *     type="string",
     *     description="La valeur à rechercher dans la base"
     * )
     *
     * @param Request $request
     * @param string $field
     * @param string $value
     * @param UserManager $userManager
     * @return bool
     *
     * @Rest\View()
     * @Rest\Get("/users/is-taken/{field}/{value}", requirements={"field"="(username|email)"})
     */
    public function getIsTaken(Request $request, string $field, string $value, UserManager $userManager)
    {
        $user = $userManager->getRepository()->findOneBy([
            $field => $value
        ]);
        return !!$user;
    }

    /**
     * Supprime un utilisateur
     * @SWG\Tag(name="Utilisateurs")
     * @SWG\Response(
     *     response=204,
     *     description="Supprime l'utilisateur, c'est irréversible"
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Un entier positif identifiant d'un utilisateur"
     * )
     * @param Request $request
     * @param $id
     * @param UserManager $userManager
     *
     * @Rest\Delete("/users/{id}", requirements={"id"="\d+"})
     */
    public function deleteUser(Request $request, $id, UserManager $userManager)
    {
        $userManager->removeUser($request, $id, $this->getUser());
    }
}