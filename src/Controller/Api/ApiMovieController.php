<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 19/06/2018
 * Time: 20:50
 */

namespace App\Controller\Api;


use App\Entity\Movie;
use App\Entity\MovieReview;
use App\Form\MovieReviewType;
use App\Form\MovieType;
use App\Manager\MovieManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class ApiMovieController extends ApiRestController
{
    /**
     * Récupère un film
     *
     * @SWG\Tag(name="Films")
     * @SWG\Response(
     *     response=200,
     *     description="Retourne un film",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Movie::class, groups={"Movie"})
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'un film, ou un identifiant imdb (format: tt0000000)"
     * )
     *
     * @Rest\View(serializerGroups={"Movie"})
     * @Rest\Get("/movies/{id}", requirements={"id"="\d+|tt\d{7}"})
     *
     * @param Request $request
     * @param $id
     * @param MovieManager $movieManager
     * @return \FOS\RestBundle\View\View
     *
     */
    public function getOneMovie(Request $request, $id, MovieManager $movieManager)
    {
        return $this->view($movieManager->findMovie($id), Response::HTTP_OK);
    }

    /**
     * Crée un film dans la base
     *
     * @SWG\Tag(name="Films")
     * @SWG\Response(
     *     response=201,
     *     description="Crée et retourne le film",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Movie::class, groups={"Movie"})
     *     )
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     type="json",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=MovieType::class)
     *     )
     * )
     *
     *
     * @Rest\View(serializerGroups={"Movie"})
     * @Rest\Post("/movies")
     *
     * @param Request $request
     * @param MovieManager $movieManager
     * @return \FOS\RestBundle\View\View
     */
    public function postMovie(Request $request, MovieManager $movieManager)
    {
        $result = $movieManager->submitFormMovie($request);
        return $this->view($result, (is_array($result) && isset($result['formError'])) ? Response::HTTP_BAD_REQUEST : Response::HTTP_OK);
    }

    /**
     * Récupère les avis sur un film
     *
     * @SWG\Tag(name="Films")
     * @SWG\Response(
     *     response=200,
     *     description="Retourne les avis sur un film",
     *     @SWG\Schema(
     *       type="array",
     *       @SWG\Items(ref=@Model(type=MovieReview::class, groups={"MovieReview"}))
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'un film, ou un identifiant imdb (format: tt0000000)"
     * )
     *
     * @Rest\View(serializerGroups={"MovieReview"})
     * @Rest\Get("/movies/{id}/reviews", requirements={"id"="\d+|tt\d{7}"})
     *
     * @param Request $request
     * @param $id
     * @param MovieManager $movieManager
     * @return \FOS\RestBundle\View\View
     *
     */
    public function getReviews(Request $request, $id, MovieManager $movieManager)
    {
        /** @var Movie $movie */
        $movie = $movieManager->findMovie($id);

        return $this->view($movie->getReviews(), Response::HTTP_OK);
    }

    /**
     * Récupère l'avis d'un utilisateur sur un film
     *
     * @SWG\Tag(name="Films")
     * @SWG\Response(
     *     response=200,
     *     description="Retourne l'avis d'un utilisateur sur un film",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=MovieReview::class, groups={"MovieReview"})
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="idMovie",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'un film, ou un identifiant imdb (format: tt0000000)"
     * )
     * @SWG\Parameter(
     *     name="idUser",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'un utilisateur, ou 'me' pour soi-même"
     * )
     *
     * @Rest\View(serializerGroups={"MovieReview"})
     * @Rest\Get("/movies/{idMovie}/reviews/{idUser}", requirements={"idMovie"="\d+|tt\d{7}", "idMovie"="\d+|me"})
     *
     * @param Request $request
     * @param $idMovie
     * @param $idUser
     * @param MovieManager $movieManager
     * @return \FOS\RestBundle\View\View
     *
     */
    public function getUserReview(Request $request, $idMovie, $idUser, MovieManager $movieManager)
    {
        /** @var Movie $movie */
        $movie = $movieManager->findMovie($idMovie);

        if ('me' === $idUser) {
            $idUser = $this->getUser()->getId();
        }

        /** @var MovieReview $review */
        foreach ($movie->getReviews() as $review) {
            if ($review->getUser()->getId() === $idUser) {
                return $this->view($review, Response::HTTP_OK);
            }
        }

        throw new NotFoundHttpException($this->translator->trans('error.http.404'));
    }

    /**
     * Crée un avis d'un film dans la base
     *
     * @SWG\Tag(name="Films")
     * @SWG\Response(
     *     response=201,
     *     description="Crée l'avis et retourne le film",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Movie::class, groups={"Movie"})
     *     )
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'un film, ou un identifiant imdb (format: tt0000000)"
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     type="json",
     *     @SWG\Schema(
     *       type="object",
     *       @SWG\Property(type="integer", property="user", description="Cette valeur est écrasée par l'utilisateur qui requête l'API"),
     *       @SWG\Property(type="integer", property="movie", description="Cette valeur est écrasée par l'identifiant passée en URL"),
     *       @SWG\Property(type="number", property="rating"),
     *       @SWG\Property(type="string", property="content")
     *     )
     * )
     *
     * @Rest\View(serializerGroups={"Movie"})
     * @Rest\Post("/movies/{id}/reviews")
     *
     * @param Request $request
     * @param MovieManager $movieManager
     * @return \FOS\RestBundle\View\View
     */
    public function addReview(Request $request, $id, MovieManager $movieManager)
    {
        /** @var Movie $movie */
        $movie = $movieManager->findMovie($id);

        $submittedData = json_decode($request->getContent(), true);
        $submittedData['user'] = $this->getUser()->getId();
        $submittedData['movie'] = $movie->getId();

        $review = new MovieReview();
        $form = $this->createForm(MovieReviewType::class, $review);
        $form->submit($submittedData, false);
        if ($form->isValid()) {
            try {
                $movie->addReview($review);
                $movieManager->persist($review, true);
            } catch (UniqueConstraintViolationException $exception) {
                throw new BadRequestHttpException($this->translator->trans('error.movie.already_reviewed'));
            }
        } else {
            return $this->view([
                'formError' => true,
                'form' => $form->getErrors()
            ], Response::HTTP_BAD_REQUEST);
        }
        return $this->view($movie, Response::HTTP_CREATED);
    }

    /**
     * Supprime l'avis de l'utilisateur d'un film dans la base
     *
     * @SWG\Tag(name="Films")
     * @SWG\Response(
     *     response=200,
     *     description="Supprime l'avis et retourne le film",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Movie::class, groups={"Movie"})
     *     )
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'un film, ou un identifiant imdb (format: tt0000000)"
     * )
     *
     * @Rest\View(serializerGroups={"Movie"})
     * @Rest\Delete("/movies/{id}/reviews")
     *
     * @param Request $request
     * @param MovieManager $movieManager
     * @return \FOS\RestBundle\View\View
     */
    public function deleteReview(Request $request, $id, MovieManager $movieManager)
    {
        /** @var Movie $movie */
        $movie = $movieManager->findMovie($id);
        $reviews = $movie->getReviews();

        /** @var MovieReview $review */
        foreach ($reviews as $review) {
            if ($review->getUser()->isEqualTo($this->getUser())) {
                $movieManager->remove($review, true);
                break;
            }
        }
        $movieManager->persist($movie, true);

        return $this->view($movie, Response::HTTP_OK);
    }
}