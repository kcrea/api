<?php
/**
 * Created by PhpStorm.
 * User: romain
 * Date: 05/03/18
 * Time: 16:48
 */

namespace App\Controller\Api;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\TranslatorInterface;


abstract class ApiRestController extends FOSRestController
{
    protected $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param Request $request
     * @param $className
     * @param $start
     * @param $nb
     *
     * @return View
     */
    protected function getEntity(Request $request, $className, $start, $nb)
    {
        $em = $this->getDoctrine()->getManager();

        $params = $request->query->all();

        $entity = $em->getRepository($className)->filter($start, $nb, $params);

        return $this->view($entity, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param $className
     *
     * @return View
     */
    protected function getCountEntity(Request $request, $className)
    {
        $em = $this->getDoctrine()->getManager();

        $params = $request->query->all();

        $entity = $em->getRepository($className)->getCount($params);

        return $this->view($entity, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param $className
     * @param $id
     *
     * @throws NotFoundHttpException
     * @return object
     */
    protected function getOneEntity(Request $request, $className, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($className)->find($id);

        $this->throwNotFoundIfNull($entity);

        return $entity;
    }

    /**
     * @param Request $request
     * @param $className
     *
     * @return object
     */
    protected function postEntity(Request $request, $className)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new $className();

        $form = $this->createForm("{$className}Type", $entity);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();
        } else {
            return $this->view($form->getErrors(), Response::HTTP_BAD_REQUEST);
        }

        return $entity;
    }
//      Only useful on non-abstract controllers
//    /**
//     * @param Request $request
//     * @param $className
//     * @param $id
//     *
//     * @throws NotFoundHttpException
//     * @return object
//     */
//    protected function putEntity(Request $request, $className, $id)
//    {
//        return $this->updateEntity($request, $className, $id, false);
//    }
//
//    /**
//     * @param Request $request
//     * @param $className
//     * @param $id
//     *
//     * @throws NotFoundHttpException
//     * @return object
//     */
//    protected function patchEntity(Request $request, $className, $id)
//    {
//        return $this->updateEntity($request, $className, $id, false);
//    }

    /**
     * @param Request $request
     * @param $className
     * @param $id
     * @param $clearMissing
     * @throws NotFoundHttpException
     * @return object|array
     */
    protected function updateEntity(Request $request, $className, $id, $clearMissing)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($className)->find($id);

        $this->throwNotFoundIfNull($entity);

        $form = $this->createForm("{$className}Type", $entity);
        $data = json_decode($request->getContent(), true);
        $form->submit($data, $clearMissing);

        if($form->isValid()) {
            $em->persist($entity);
            $em->flush();
        }else{
            return [
                'formError' => true,
                'form' => $form->getErrors()
            ];
        }
        return $entity;
    }

    /**
     * @param Request $request
     * @param $className
     * @param $id
     *
     * @throws NotFoundHttpException
     * @return void
     */
    protected function deleteEntity(Request $request, $className, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($className)->find($id);

        $this->throwNotFoundIfNull($entity);

        $em->remove($entity);
        $em->flush();
    }

    protected function throwNotFoundIfNull($object) {
        if (null === $object) {
            throw new NotFoundHttpException($this->translator->trans('error.http.404'));
        }
    }
}
