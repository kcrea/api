<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 13/04/2018
 * Time: 10:36
 */

namespace App\Controller\Api;


use App\Entity\Library;
use App\Entity\Movie;
use App\Manager\LibraryManager;
use App\Manager\MovieManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ApiLibraryController extends ApiRestController
{
    /**
     * Liste les médiathèques publiques
     *
     * @SWG\Tag(name="Médiathèques")
     * @SWG\Response(
     *     response=200,
     *     description="Retourne une liste de médiathèques publiques et leurs dépendances les plus importantes",
     *     @SWG\Schema(
     *       type="array",
     *       @SWG\Items(type="object", ref=@Model(type=Library::class, groups={"Library"}))
     *     )
     * )
     *
     * @Rest\View(serializerGroups={"Library"})
     * @Rest\Get("/libraries/public")
     *
     * @param Request $request
     * @param LibraryManager $libraryManager
     * @return View
     */
    public function getPublicLibraries(Request $request, LibraryManager $libraryManager)
    {
        return $this->view($libraryManager->findPublic(), Response::HTTP_OK);
    }

    /**
     * Récupère une médiathèque
     *
     * @SWG\Tag(name="Médiathèques")
     * @SWG\Response(
     *     response=200,
     *     description="Retourne une médiathèque et ses dépendances les plus importantes",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Library::class, groups={"Library"})
     *     )
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'une médiathèque ou 'me' pour voir sa médiathèque"
     * )
     *
     * @Rest\View(serializerGroups={"Library"})
     * @Rest\Get("/libraries/{id}", requirements={"id"="(\d+|me)"})
     *
     * @param Request $request
     * @param $id
     * @param LibraryManager $libMng
     * @return View
     */
    public function getOneLibrary(Request $request, $id, LibraryManager $libMng)
    {
        if ('me' === $id) {
            if (null === $this->getUser()) { // e.g. anon
                throw new UnauthorizedHttpException(
                    $this->translator->trans('error.http.401_challenge'),
                    $this->translator->trans('error.library.me_anon')
                );
            }

            return $this->view($this->getUser()->getLibrary(), Response::HTTP_OK);
        }

        $library = parent::getOneEntity($request, Library::class, $id);
        $libMng->throwNotFoundIfPrivate($library);
        return $this->view($library, Response::HTTP_OK);
    }

    /**
     * Change la visibilité de la médiathèque
     * @SWG\Tag(name="Médiathèques")
     * @SWG\Response(
     *     response=200,
     *     description="Si la visibilité est publique, passe en privé, et inversement, et retourne la médiathèque",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Library::class, groups={"Library"})
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'une médiathèque ou 'me' pour voir sa médiathèque"
     * )
     * @param Request $request
     * @param $id
     * @param LibraryManager $libMng
     * @return View
     *
     * @Rest\View(serializerGroups={"Library"})
     * @Rest\Patch("/libraries/{id}/switch-privacy", requirements={"id"="(\d+|me)"})
     */
    public function patchSwitchLibraryPrivacy(Request $request, $id, LibraryManager $libMng)
    {
        if ('me' === $id) {
            $library = $this->getUser()->getLibrary();
        } else {
            /** @var Library|null $library */
            $library = parent::getOneEntity($request, Library::class, $id);
        }
        $libMng->canEdit($library, $this->getUser());

        $library->setPublic(!$library->isPublic());
        $libMng->persist($library, true);
        return $this->view($library, Response::HTTP_OK);
    }

    /**
     * Ajoute un film à une médiathèque
     *
     * @SWG\Tag(name="Médiathèques")
     * @SWG\Response(
     *     response=201,
     *     description="Ajoute le film à la médiathèque",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Library::class, groups={"Library"})
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="idLibrary",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'une médiathèque ou 'me' pour voir sa médiathèque"
     * )
     * @SWG\Parameter(
     *     name="idMovie",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'un film ou un identifiant imdb (format: tt0000000)"
     * )
     *
     * @Rest\View(serializerGroups={"Library"})
     * @Rest\Post("/libraries/{idLibrary}/movies/{idMovie}", requirements={"idLibrary" = "\d+", "idMovie" = "\d+|tt\d{7}"})
     *
     * @param Request $request
     * @param $idLibrary
     * @param $idMovie
     * @param LibraryManager $libMng
     * @param MovieManager $mvMng
     *
     * @return View
     *
     * @throws BadRequestHttpException
     */
    public function postAddMovieToLibrary(Request $request, $idLibrary, $idMovie, LibraryManager $libMng, MovieManager $mvMng)
    {
        if ('me' === $idLibrary) {
            $library = $this->getUser()->getLibrary();
        } else {
            /** @var Library|null $library */
            $library = parent::getOneEntity($request, Library::class, $idLibrary);
        }
        $libMng->canEdit($library, $this->getUser());
        /** @var Movie $movie */
        $movie = $mvMng->findMovie($idMovie);

        $library->addMovie($movie);

        try {
            $libMng->persist($library, true);
        } catch (UniqueConstraintViolationException $exception) {
            throw new BadRequestHttpException($this->translator->trans('error.action_already_done'));
        }

        return $this->view($library, Response::HTTP_OK);
    }

    /**
     * Supprime le film d'une médiathèque
     *
     * @SWG\Tag(name="Médiathèques")
     * @SWG\Response(
     *     response=200,
     *     description="Supprime le film à la médiathèque",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Library::class, groups={"Library"})
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="idLibrary",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'une médiathèque ou 'me' pour voir sa médiathèque"
     * )
     * @SWG\Parameter(
     *     name="idMovie",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'un film ou un identifiant imdb (format: tt0000000)"
     * )
     *
     * @Rest\View(serializerGroups={"Library"})
     * @Rest\Delete("/libraries/{idLibrary}/movies/{idMovie}", requirements={"idLibrary" = "\d+", "idMovie" = "\d+|tt\d{7}"})
     *
     * @param Request $request
     * @param MovieManager $movieManager
     * @return \FOS\RestBundle\View\View
     */
    public function deleteMovieFromLibrary(Request $request, $idLibrary, $idMovie, LibraryManager $libMng, MovieManager $mvMng)
    {
        if ('me' === $idLibrary) {
            $library = $this->getUser()->getLibrary();
        } else {
            /** @var Library|null $library */
            $library = parent::getOneEntity($request, Library::class, $idLibrary);
        }
        $libMng->canEdit($library, $this->getUser());
        /** @var Movie $movie */
        $movie = $mvMng->findMovie($idMovie);

        $library->removeMovie($movie);

        try {
            $libMng->persist($library, true);
        } catch (UniqueConstraintViolationException $exception) {
            throw new BadRequestHttpException($this->translator->trans('error.action.already_done'));
        }

        return $this->view($library, Response::HTTP_OK);
    }
}