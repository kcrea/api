<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 18/01/2018
 * Time: 10:00
 */

namespace App\Controller\Api;


use FOS\RestBundle\Util\ExceptionValueMap;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use FOS\RestBundle\Controller\ExceptionController as FOSExceptionController;

class ApiExceptionController extends FOSExceptionController
{
    private $viewHandler;

    public function __construct(ViewHandlerInterface $viewHandler, ExceptionValueMap $exceptionCodes, $showException)
    {
        $this->viewHandler = $viewHandler;
        parent::__construct($viewHandler, $exceptionCodes, $showException);
    }

    /**
     * Converts an Exception to a Response.
     *
     * @param Request $request
     * @param \Exception|\Throwable $exception
     * @param DebugLoggerInterface|null $logger
     *
     * @throws \InvalidArgumentException
     *
     * @return Response
     */
    public function showAction(Request $request, $exception, DebugLoggerInterface $logger = null)
    {
        if (ob_get_level() <= $request->headers->get('X-Php-Ob-Level', -1)) {
            $currentContent = '';
        } else {
            Response::closeOutputBuffers($request->headers->get('X-Php-Ob-Level', -1) + 1, true);
            $currentContent = ob_get_clean();
        }

        $code = $this->getStatusCode($exception);

        $templateData = [
            'exception' => $exception,
            'status' => 'error',
            'status_code' => $code,
            'status_text' => array_key_exists($code, Response::$statusTexts) ? Response::$statusTexts[$code] : 'error',
            'currentContent' => $currentContent,
            'logger' => $logger,
        ];

        $view = $this->createView($exception, $code, $templateData, $request, true);
        $response = $this->viewHandler->handle($view);

        return $response;
    }
}