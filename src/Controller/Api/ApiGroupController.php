<?php
/**
 * Created by PhpStorm.
 * Group: rvale
 * Date: 09/04/2018
 * Time: 09:35
 */

namespace App\Controller\Api;


use App\Entity\Library;
use App\Entity\Group;
use App\Entity\UserGroup;
use App\Form\GroupType;
use App\Manager\LibraryManager;
use App\Manager\GroupManager;
use App\Manager\UserManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ApiGroupController extends ApiRestController
{
    /**
     * Recherche de groupes
     *
     * @SWG\Tag(name="Groupes")
     * @SWG\Response(
     *     response=200,
     *     description="Retourne une liste de groupes et de leurs dépendances les plus importantes",
     *     @SWG\Schema(
     *       type="array",
     *       @SWG\Items(type="object", ref=@Model(type=Group::class, groups={"Group"}))
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="label",
     *     in="query",
     *     type="string",
     *     description="Une chaîne de caractère de recherche de groupe"
     * )
     *
     * @Rest\View(serializerGroups={"Group"})
     * @Rest\Get("/groups")
     *
     * @param Request $request
     * @param GroupManager $groupManager
     * @return View
     */
    public function getSearchGroup(Request $request, GroupManager $groupManager)
    {
        try {
            $entities = $groupManager->search($request->query->all());
            return $this->view($entities, Response::HTTP_OK);
        } catch (\Exception $exception) {
            throw new BadRequestHttpException($this->translator->trans('error.group.wrong_parameters'));
        }
    }

    /**
     * Récupère les informations d'un groupe
     *
     * @SWG\Tag(name="Groupes")
     * @SWG\Response(
     *     response=200,
     *     description="Retourne un groupe et ses dépendances les plus importantes",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Group::class, groups={"Group"})
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Un entier positif identifiant d'un groupe"
     * )
     *
     * @Rest\View(serializerGroups={"Group"})
     * @Rest\Get("/groups/{id}", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param $id
     * @return View
     */
    public function getOneGroup(Request $request, $id)
    {
        return $this->view(parent::getOneEntity($request, Group::class, $id), Response::HTTP_OK);
    }

    /**
     * Crée un groupe
     * @SWG\Tag(name="Groupes")
     * @SWG\Response(
     *     response=201,
     *     description="Crée et retourne le groupe",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Group::class, groups={"Group"})
     *     )
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     type="json",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=GroupType::class)
     *     )
     * )
     * @param Request $request
     * @param GroupManager $groupManager
     * @return View
     *
     * @Rest\View(serializerGroups={"Group"})
     * @Rest\Post("/groups")
     */
    public function postCreateGroup(Request $request, GroupManager $groupManager)
    {
        $result = $groupManager->createGroup($request, $this->getUser());
        if (!(is_array($result) && isset($result['formError']))) {
//            $library = $libMng->createLibrary($result);
            $library = new Library();
            $library->setPublic(false);
            $library->setGroup($result);
            $result->addUser($this->getUser(), true);
            $result->setLibrary($library);
            $groupManager->merge($result, true);
        }
        return $this->view($result, (is_array($result) && isset($result['formError'])) ?
            Response::HTTP_BAD_REQUEST :
            Response::HTTP_CREATED);
    }

    /**
     * Met à jour un groupe
     * @SWG\Tag(name="Groupes")
     * @SWG\Response(
     *     response=200,
     *     description="Met à jour et retourne le groupe",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Group::class, groups={"Group"})
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Un entier positif identifiant d'un groupe"
     * )
     * @SWG\Parameter(
     *     name="form",
     *     in="body",
     *     type="json",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=GroupType::class)
     *     )
     * )
     * @param Request $request
     * @param int $id
     * @param GroupManager $userManager
     * @return View
     *
     * @Rest\View(serializerGroups={"Group"})
     * @Rest\Put("/groups/{id}", requirements={"id"="\d+"})
     * @Rest\Patch("/groups/{id}", requirements={"id"="\d+"})
     */
    public function updateGroup(Request $request, int $id, GroupManager $userManager)
    {
        $result = $userManager->updateGroup($request, $id, $this->getUser());
        return $this->view($result, (is_array($result) && isset($result['formError'])) ? Response::HTTP_BAD_REQUEST : Response::HTTP_OK);
    }

    /**
     * Supprime un groupe
     * @SWG\Tag(name="Groupes")
     * @SWG\Response(
     *     response=204,
     *     description="Supprime le groupe, c'est irréversible"
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Un entier positif identifiant d'un groupe"
     * )
     * @param Request $request
     * @param $id
     * @param GroupManager $userManager
     *
     * @Rest\Delete("/groups/{id}", requirements={"id"="\d+"})
     */
    public function deleteGroup(Request $request, $id, GroupManager $userManager)
    {
        $userManager->removeGroup($request, $id, $this->getUser());
    }

    /**
     * Rejoindre un groupe
     *
     * @SWG\Tag(name="Groupes")
     * @SWG\Response(
     *     response=201,
     *     description="L'utilisateur authentifié rejoint le groupe et retourne le groupe",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Group::class, groups={"Group"})
     *     )
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'un groupe"
     * )
     *
     * @Rest\View(serializerGroups={"Group"})
     * @Rest\Post("/groups/{id}/join")
     *
     * @param Request $request
     * @param GroupManager $groupManager
     * @return \FOS\RestBundle\View\View
     */
    public function postJoinGroup(Request $request, $id, GroupManager $groupManager)
    {
        /** @var Group $group */
        $group = $groupManager->findGroup($id, $this->getUser());

        $group->addUser($this->getUser());
        try {
            $groupManager->persist($group, true);
        } catch (UniqueConstraintViolationException $exception) {
            throw new BadRequestHttpException($this->translator->trans('error.group.already_in'));
        }
        return $this->view($group, Response::HTTP_CREATED);
    }

    /**
     * Quitter le groupe
     *
     * @SWG\Tag(name="Groupes")
     * @SWG\Response(
     *     response=200,
     *     description="Quitte le groupe et retourne le groupe",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Group::class, groups={"Group"})
     *     )
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'un groupe"
     * )
     *
     * @Rest\View(serializerGroups={"Group"})
     * @Rest\Delete("/groups/{id}/leave")
     *
     * @param Request $request
     * @param GroupManager $groupManager
     * @return \FOS\RestBundle\View\View
     */
    public function deleteLeaveGroup(Request $request, $id, GroupManager $groupManager)
    {
        /** @var Group $group */
        $group = $groupManager->findGroup($id, $this->getUser());

        $groupManager->throwIfCreator($group, $this->getUser());

        /** @var UserGroup $userGroup */
        foreach ($group->getUsers() as $userGroup) {
            if ($userGroup->getUser()->isEqualTo($this->getUser())) {
                $groupManager->remove($userGroup, true);
                break;
            }
        }
        $groupManager->persist($group, true);

        return $this->view($group, Response::HTTP_OK);
    }

    /**
     * Change l'administrabilité d'un utilisateur
     * @SWG\Tag(name="Groupes")
     * @SWG\Response(
     *     response=200,
     *     description="Donne la valeur inverse de la possibilité d'administration d'un utilisateur",
     *     @SWG\Schema(
     *       type="object",
     *       ref=@Model(type=Group::class, groups={"Group"})
     *     )
     * )
     * @Security(name="ApiKey")
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="string",
     *     description="Un entier positif identifiant d'un groupe"
     * )
     * @param Request $request
     * @param $id
     * @param GroupManager $groupManager
     * @return View
     *
     * @Rest\View(serializerGroups={"Group"})
     * @Rest\Patch("/groups/{idGroup}/users/{idUser}/switch-admin", requirements={"idGroup"="\d+", "idUser"="\d+"})
     */
    public function patchSwitchUserAdmin(Request $request, $idGroup, $idUser, GroupManager $groupManager)
    {
        $group = $groupManager->findGroup($idGroup, $this->getUser(), true);

        $userGroup = $groupManager->findUserGroup($group, $idUser);

        $userGroup->setAdmin(!$userGroup->isAdmin());
        $groupManager->persist($userGroup, true);
        $this->getDoctrine()->getManager()->refresh($group);
        return $this->view($group, Response::HTTP_OK);
    }

    /**
     *
     *
     * @param Request $request
     * @param $idGroup
     * @param $idUser
     * @param GroupManager $groupManager
     * @param UserManager $userManager
     *
     * @Rest\View(serializerGroups={"Group"})
     * @Rest\Delete("/groups/{idGroup}/users/{idUser}", requirements={"idGroup"="\d+", "idUser"="\d+"})
     */
    public function deleteExpelUser(Request $request, $idGroup, $idUser, GroupManager $groupManager, UserManager $userManager)
    {
        $group = $groupManager->findGroup($idGroup, $this->getUser(), true);

        $user = $userManager->getRepository()->find($idUser);

        $groupManager->throwIfCreator($group, $user);

        /** @var UserGroup $userGroup */
        foreach ($group->getUsers() as $userGroup) {
            if ($userGroup->getUser()->isEqualTo($user)) {
                $groupManager->remove($userGroup, true);
                break;
            }
        }
        $groupManager->persist($group, true);
    }
}