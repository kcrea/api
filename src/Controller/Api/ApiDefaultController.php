<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 05/04/2018
 * Time: 21:43
 */

namespace App\Controller\Api;


use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;

class ApiDefaultController extends ApiRestController
{
    /**
     * Bonjour !
     *
     * @SWG\Tag(name="Autres")
     * @SWG\Response(
     *     response=200,
     *     description="Retourne 'hello' lorsque l'API est fonctionnelle",
     *     @SWG\Schema(type="string")
     * )
     *
     * @Rest\View()
     * @Rest\Get("/hello")
     */
    public function getHello()
    {
        return 'hello';
    }
}