<?php
/**
 * Created by PhpStorm.
 * User: rvale
 * Date: 16/04/2018
 * Time: 16:04
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function index()
    {
        return new Response('<p>Hello World!</p>', 200);
    }
}