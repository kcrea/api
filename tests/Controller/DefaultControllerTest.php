<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 07/04/2018
 * Time: 15:24
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testGetHello()
    {
        $client = static::createClient();
        $client->request('GET', '/api/hello');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}